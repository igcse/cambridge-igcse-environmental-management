# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This repository is to hold the "Cambridge IGCSE Environmental Management" UAT package
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
When delivered to ES Support team for movement through CPD-181, the content was copied to the directory "clp-uat-repo//Content/UAT/JI/Leckie/IGCSE-Environmental-Management-SB-17-Jan-V1/". Antony from ES created the directory structure "clp-uat-repo/Content/secondary/igcse/cambridge/cambridge_igcse_environmental_management/" to hold the content from there on.
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

When delivered to ES Support team for movement through CPD-181, the content was copied to the directory "clp-uat-repo//Content/UAT/JI/Leckie/IGCSE-Environmental-Management-SB-17-Jan-V1/". Antony from ES created the directory structure "clp-uat-repo/Content/secondary/igcse/cambridge/cambridge_igcse_environmental_management/" to hold the content from there on.

Below given directories are already seen in UAT repository, other than what came with the package "ES_IGCSE-Environmental-Management-SB_V2"

IGCSE-Environmental-Management-SB-04-Jan-V1/
IGCSE-Environmental-Management-SB-11-Jan-V1/
IGCSE-Environmental-Management-SB-13-Jan-V1-TEST/
IGCSE-Environmental-Management-SB-13-Jan-V1/

ANYONE WHO WORK ON THE PACKAGE SHOULD BE USING "clp-uat-repo/Content/secondary/igcse/cambridge/cambridge_igcse_environmental_management/" at any given point in time. This is to avoid the confusion that may be created when multiple teams are working on the same project (package)

### Version changes ###

* V1 (As received through CPD-181)

1) Content was pointing to "clp-uat-repo//Content/UAT/JI/Leckie/IGCSE-Environmental-Management-SB-17-Jan-V1/"
2) Wrong conver image is part of the package

* V2 (Uploaded by ES to UAT)
1) Content path was changed from "clp-uat-repo//Content/UAT/JI/Leckie/IGCSE-Environmental-Management-SB-17-Jan-V1/" to "clp-uat-repo/Content/secondary/igcse/cambridge/cambridge_igcse_environmental_management/"

### Who do I talk to? ###

* Repo owner or admin
Antony Bineesh (Bineesh K Thomas @ antonybineesh@excelindia.com) from Excel Soft Technologies Pvt. Ltd (Ultimately the property of HarperCollins UK)
* Other community or team contact
Rachel Allegro (@ Rachel.Allegro@harpercollins.co.uk)